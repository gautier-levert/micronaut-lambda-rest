package test.app

import io.micronaut.runtime.Micronaut.build

fun main(args: Array<String>) {
    build(*args)
            .packages("test.app")
            .start()
}

