package test.app

import com.amazonaws.serverless.exceptions.ContainerInitializationException
import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.RequestStreamHandler
import io.micronaut.function.aws.proxy.MicronautLambdaContainerHandler
import io.micronaut.runtime.Micronaut
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream

class StreamLambdaHandler : RequestStreamHandler {

    private val log = getLogger<StreamLambdaHandler>()

    private var handler = try {
        MicronautLambdaContainerHandler(
                Micronaut.build()
                        .packages("test.app")
        )
    } catch (e: ContainerInitializationException) {
        // if we fail here. We re-throw the exception to force another cold start
        log.error("", e)
        throw RuntimeException("Could not initialize Micronaut", e)
    }

    @Throws(IOException::class)
    override fun handleRequest(inputStream: InputStream, outputStream: OutputStream, context: Context) {
        handler.proxyStream(inputStream, outputStream, context)
    }
}
